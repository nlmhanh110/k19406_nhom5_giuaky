module.exports = {
        BTN_LOGOUT:'//span[text()="Đăng xuất"]' ,
        TXT_WEBSITE:"[formcontrolname='website']",
        TXT_NAME:"[formcontrolname='name']",
        RADIO_FEATURE:"[formcontrolname='segment_feature']",
        BTN_PROFILE_MENU:"#profile_menu",
        NAVIGATION:"[role='navigation']",
        BTN_CREATE_API_KEYS:"[class='api-keys-header-action']",
        SETTING_TAB:"//span[contains(@class,'mat-content')]//p[contains(text(),'Thiết lập')]",
        API_KEYS_TAB:"//a[contains(@class,'mat-list-item')]//span[contains(text(),'Api keys')]",
        ANOTHER_SETTING_TAB:"//a[contains(@class,'mat-list-item')]//span[contains(text(),'Thiết lập khác')]"
}