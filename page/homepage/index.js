const {I} = inject()
const homePageLocator = require('./locator')
module.exports={
    createNewBusinessProfile(website,name,feature){
        I.click('Tạo một doanh nghiệp')
        I.fillField(homePageLocator.TXT_WEBSITE,website)
        I.fillField(homePageLocator.TXT_NAME,name)
        I.click(feature,homePageLocator.RADIO_FEATURE)
        I.click('Tạo doanh nghiệp')
    },
    verifyCreateBusinessProfileSuccessfully(){
        I.seeInCurrentUrl('/business')

    }
}
