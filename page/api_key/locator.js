module.exports = {
    TXT_API_NAME: "[formcontrolname='name']",
    BTN_CREATE_API: "[class='submit']",
    TOAST_CONTAINER: "#toast-container",
    BTN_EDIT_API: "[mattooltip='Sửa']",
    BTN_DELETE_API: "[mattooltip='Xóa']",
    DIV_DELETE_COMPANY: "//div[@class='settings-others-content']//mat-panel-title[contains(text(),'Xóa doanh nghiệp')]",
    BTN_DELETE_COMPANY: "//button//span[contains(text(),'Xóa doanh nghiệp')]",
    BTN_CONFIRM_DELETE: "//button//span[contains(text(),'Tôi hiểu điều này, xóa doanh nghiệp')]",
    TXT_CONFIRM_COMPANY:"[formcontrolname='confirm']"
}