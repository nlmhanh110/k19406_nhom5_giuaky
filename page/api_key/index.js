const {I} = inject()
const apiPageLocator = require('../api_key/locator')
const homePageLocator = require('../homepage/locator')
const timeout = require("../common/timeout")
module.exports={
    createAPIKeys(name){
        I.waitForElement(homePageLocator.SETTING_TAB,timeout.loading)
        I.click("Thiết lập",homePageLocator.SETTING_TAB)
        I.waitForElement(homePageLocator.API_KEYS_TAB,timeout.loading)
        I.click(homePageLocator.API_KEYS_TAB)
        I.click('Tạo API key',homePageLocator.BTN_CREATE_API_KEYS)
        I.fillField(apiPageLocator.TXT_API_NAME,name)
        I.click('Tạo và xem API Key',apiPageLocator.BTN_CREATE_API)
        I.waitForText('API Key đã được khởi tạo thành công')
    },
    editAPIKey(name){
        I.click(apiPageLocator.BTN_EDIT_API)
        I.fillField(apiPageLocator.TXT_API_NAME,name)
        I.click('Lưu thay đổi',apiPageLocator.BTN_CREATE_API)
    },
    deleteAPIKey(){
        I.click(apiPageLocator.BTN_DELETE_API)
        I.click("Đồng ý")
    },
    deleteCompany(name){
        I.click(homePageLocator.ANOTHER_SETTING_TAB)
        I.waitForElement(homePageLocator.ANOTHER_SETTING_TAB,timeout.loading)
        I.click(apiPageLocator.DIV_DELETE_COMPANY)
        I.click(apiPageLocator.BTN_DELETE_COMPANY)
        I.fillField(apiPageLocator.TXT_CONFIRM_COMPANY,name)
        I.click(apiPageLocator.BTN_CONFIRM_DELETE)
        I.waitForText('Hiện tại tài khoản của bạn chưa có "Doanh nghiệp" nào. Bạn vui lòng bấm vào nút phía dưới để có thể tạo "Doanh nghiệp"!',timeout.loading)
    },
    verifyCreateAPIsuccessfully(name){
        I.see("Tạo API Key thành công.", apiPageLocator.TOAST_CONTAINER)
        I.see('API Key đã được khởi tạo thành công')
        I.click('Xong')
    },
    verifyEditAPISuccessfully(name){
        I.see("Cập nhật API Key thành công.", apiPageLocator.TOAST_CONTAINER)
        I.see(name)
    },
    verifyDeleteAPISuccessfully(name){
        I.see("Xóa API Key thành công", apiPageLocator.TOAST_CONTAINER)
        I.dontSee(name)
    }
}
