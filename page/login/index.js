const {I} = inject()
const { TXT_EMAIL, TXT_PASSWORD, BTN_LOGIN, url } = require('../login/locator')
const homePageLocator = require('../homepage/locator')
const timeout = require('../common/timeout')
module.exports={
    logIn(email,password){
        I.amOnPage(url)
        I.fillField(TXT_EMAIL,email)
        I.fillField(TXT_PASSWORD,password)
        I.click(BTN_LOGIN)
    },
    verifyLogInSuccessfully(){
        I.see('Hiện tại tài khoản của bạn chưa có "Doanh nghiệp" nào. Bạn vui lòng bấm vào nút phía dưới để có thể tạo "Doanh nghiệp"!')
        I.seeInCurrentUrl('https://crosstech-sandbox.casso.vn/portfolio')
    },
    verifyInvalidEmail(){
        I.see('Email không hợp lệ!')
        I.seeInCurrentUrl(url)
        I.dontSee('Hiện tại tài khoản của bạn chưa có "Doanh nghiệp" nào. Bạn vui lòng bấm vào nút phía dưới để có thể tạo "Doanh nghiệp"!')
    }
}
