Feature: Business for login
    In order to achieve my goals
    As a persona
    I want to be able to login
    Scenario: Check login flow when using valid data
        When I login to Casso page using email "nguyenlemyhanh12@gmail.com" and password "WN3Rv5XR8FAyS-."
        Then I can see portfolio page
    Scenario: User log in using invalid email
        When I login to Casso page using email "hanh@gmail@.com" and password "123456"
        Then I can see warning message for invalid email