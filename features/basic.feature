Feature: Business for login, create business profile and manage API keys
  In order to achieve my goals
  As a persona
  I want to be able to login,create business profile and manage API keys
  Scenario: Check Login, Create business profile and manage API keys flows work successfully with valid data
    When I login to Casso page using email "nguyenlemyhanh12@gmail.com" and password "WN3Rv5XR8FAyS-."
    Then I can see portfolio page
    When I create new business profile using website "https://nhom5.vn" and name "nhom5" and feature "Khác"
    Then I can create and see created business profile
    When I create new API keys with name "new_api"
    Then I can see successful notification and created API with name "new_api"
    When I edit API key with name "edited_api"
    Then I can see successful notification and edited API with name "edited_api"
    When I delete API key with name "edited_api"
    Then I can see successful notification and can not see deleted API with name "edited_api"
    Then I delete company with name "nhom5"