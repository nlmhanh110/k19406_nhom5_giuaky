const { I } = inject();
const logInPage = require('../page/login/index')
Given('I login to Casso page',async ()=>{
    logInPage.logIn(email,password)
})
When('I login to Casso page using email {string} and password {string}',async (email,password)=>{
    logInPage.logIn(email,password)
})
Then('I can see portfolio page',async ()=>{
    logInPage.verifyLogInSuccessfully()
})