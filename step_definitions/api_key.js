const {I} = inject()
const apiPage = require('../page/api_key/index')

When('I create new API keys with name {string}',async (name)=>{
    apiPage.createAPIKeys(name)
})
Then('I can see successful notification and created API with name {string}',async(name)=>{
    apiPage.verifyCreateAPIsuccessfully(name)
})

When('I edit API key with name {string}',async (name)=>{
    apiPage.editAPIKey(name)
})
Then('I can see successful notification and edited API with name {string}',async(name)=>{
    apiPage.verifyEditAPISuccessfully(name)
})
When('I delete API key with name {string}',async (name)=>{
    apiPage.deleteAPIKey()
})
Then('I can see successful notification and can not see deleted API with name {string}',async(name)=>{
    apiPage.verifyDeleteAPISuccessfully(name)
})
Then('I delete company with name {string}',async(name)=>{
    apiPage.deleteCompany(name)
})