const {I} = inject()
const homePage = require('../page/homepage/index')

When('I create new business profile using website {string} and name {string} and feature {string}',async (website,name,feature)=>{
    homePage.createNewBusinessProfile(website,name,feature)
})
Then('I can create and see created business profile',async()=>{
    homePage.verifyCreateBusinessProfileSuccessfully()
})